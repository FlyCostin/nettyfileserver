package com.rrkd.handler;

import com.rrkd.model.RequestFile;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * @author WZQ
 *针对Netty读快写慢的操作，导致内存溢出的问题所创建的一个handler
 */
public class PipelineReadAndWriteHandler  extends ChannelInboundHandlerAdapter{
	
	
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		if(msg instanceof RequestFile){
			RequestFile request=(RequestFile) msg;
			ctx.write(request);
			if(!ctx.channel().isWritable()){
				ctx.channel().config().setAutoRead(false);
			}
		}
		
		
	}
	
	@Override
	public void channelWritabilityChanged(ChannelHandlerContext ctx) {
		
		if(!ctx.channel().isWritable()){
			ctx.channel().config().setAutoRead(true);
		}
	}
	
}

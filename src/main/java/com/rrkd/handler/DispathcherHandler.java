package com.rrkd.handler;


import com.rrkd.model.RequestFile;
import com.rrkd.server.StatusInterface;
import com.rrkd.util.Status;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPipeline;

public class DispathcherHandler extends ChannelInboundHandlerAdapter  {

	private RequestFile request;
	@Override
	public void channelRead(ChannelHandlerContext ctx,Object msg){
		if(msg instanceof RequestFile){
			ChannelPipeline pipeline=ctx.pipeline();
			request=(RequestFile) msg;
			String uploadstatus=request.getUpload_status();
			getPipeline(uploadstatus,pipeline);//设置pipeline
			
			ctx.fireChannelRead(request);
			
		}
	}
	@SuppressWarnings("unused")
	public void getPipeline(String uploadstatus,ChannelPipeline pipeline){
		ChannelHandler dishandler=null;
		ChannelHandler transfer=pipeline.get("transfer");;
		ChannelHandler spilt=pipeline.get("spilt");
		ChannelHandler pointhandler=pipeline.get("point");
		ChannelHandler scheduleHandler=pipeline.get("schedule");


		if(uploadstatus!=null){
			if(uploadstatus.equals("Scheduledtime")){
				//pipeline.addLast("schedule",new FileCheckSM3Handler());transfer
				
				if(transfer!=null&&scheduleHandler==null){
					pipeline.addBefore("transfer", "schedule", new FileCheckSM3Handler());
					pointhandler=pipeline.get("point");
					if(pointhandler!=null){
						pipeline.remove("point");
					}
					
				}else{
					
					if(spilt!=null){
						pipeline.remove("spilt");
						pipeline.addLast("transfer",new FileTransferServerHandler());
						pipeline.addBefore("transfer", "schedule", new FileCheckSM3Handler());
					}
					
					
				}
				
			}else if(uploadstatus.equals("point")){
				
				if(transfer!=null&&pointhandler==null){
					pipeline.addBefore("transfer", "point", new FileTransferByPointHandler());
					scheduleHandler=pipeline.get("schedule");
					if(scheduleHandler!=null){
						pipeline.remove("schedule");
					}
					
				}else{
					if(spilt!=null){
						pipeline.remove("spilt");
						pipeline.addLast("transfer",new FileTransferServerHandler());
						pipeline.addBefore("transfer", "point", new FileTransferByPointHandler());
					}
					
				}
				
				
			}else if(uploadstatus.equals("spilt")){
					if(spilt==null){
						pipeline.addLast("spilt", new BigFileBySpiltServerHandler());
						pipeline.remove("transfer");
						if(pointhandler!=null){
							pipeline.remove("point");
						}else if(scheduleHandler!=null){
							pipeline.remove("schedule");
						}
					}
			
				}else if(uploadstatus.equals(StatusInterface.START_ZL_UPLOAD)){//当状态为增量传输时，就意味着是一般性质的传输
					pipeline.remove("schedule");
					if(spilt!=null){
						pipeline.remove("spilt");
						
					}
					if(pointhandler!=null){
						pipeline.remove("point");
					}
				}else if(uploadstatus.equals(StatusInterface.UPLOAD_POINT)){
					pipeline.remove("point");
					if(spilt!=null){
						pipeline.remove("spilt");
						
					}
					if(scheduleHandler!=null){
						pipeline.remove("schedule");
					}
				}
			
		}
		
	}
}

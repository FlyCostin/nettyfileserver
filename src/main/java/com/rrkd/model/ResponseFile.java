package com.rrkd.model;

import java.io.Serializable;

import com.rrkd.util.Status;

public class ResponseFile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1425307876096494974L;

	public ResponseFile() {

	}

	public ResponseFile(long start, String file_sm3, String file_url,String result) {
		super();
		this.start = start;
		this.file_sm3 = file_sm3;
		this.file_url = file_url;
		this.end = true;
		this.progress = 100;
		this.result = result;
		
	}

	public ResponseFile(long start, String file_sm3, long progress) {
		super();
		this.start = start;
		this.file_sm3 = file_sm3;
		this.end = false;
		this.progress = (int) progress;
		
	}

	public ResponseFile(String fileurl,String cause){
		super();
		this.file_url=fileurl;
		this.end = true;
		this.cause=cause;
	}
	/**
	 * 开始 读取点
	 */
	private long start;
	/**
	 * 文件的 SM3值
	 */
	private String file_sm3;
	/**
	 * 文件下载地址
	 */
	private String file_url;
	/**
	 * 上传是否结束
	 */
	private boolean end;
	/**
	 * 进度
	 */
	private int progress;

	private long filesize;

	private String result;

	private String fileName;

	private String cause;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFilesize() {
		return filesize;
	}

	public void setFilesize(long filesize) {
		this.filesize = filesize;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public long getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public String getFile_sm3() {
		return file_sm3;
	}

	public void setFile_sm3(String file_sm3) {
		this.file_sm3 = file_sm3;
	}

	public String getFile_url() {
		return file_url;
	}

	public void setFile_url(String file_url) {
		this.file_url = file_url;
	}

	public boolean isEnd() {
		return end;
	}

	public void setEnd(boolean end) {
		this.end = end;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

}

package com.rrkd.model;

import java.io.File;
import java.io.Serializable;
import java.util.List;

public class RequestFile implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1425307876096494974L;
	private File file;// 文件
	private String file_name;// 文件名
	private long starPos;// 开始位置
	private byte[] bytes;// 文件字节数组
	private int endPos;// 结尾位置
	private String file_md5; //文件的MD5值
	private String file_type;  //文件类型
	private long localfile_size; //文件总长度
	private long remotefile_size;//服务端文件总长度
	private String Parent;//上一层路径
	private String sourceParent;//源文件的上一层路径
	private boolean attachment;//压缩文件接收
	private String SM3;//国密加密
	private boolean sm4;//国密4加密之后的字节流
	private String upload_status;//上传状态
	private String sourceFiletype;//源文件的类型
	private List<RequestFile> tempFiles;//分块文件
	private int tempsize;//分块文件的总个数
	private String Crc;//文件差错性校验
	private String SM3MatchResult;
	private String uploadDestPath;
	private String sourcefilename;
	private int mark;
	
	
	
	public int getMark() {
		return mark;
	}

	public void setMark(int mark) {
		this.mark = mark;
	}

	public String getSourcefilename() {
		return sourcefilename;
	}

	public void setSourcefilename(String sourcefilename) {
		this.sourcefilename = sourcefilename;
	}

	public String getUploadDestPath() {
		return uploadDestPath;
	}

	public void setUploadDestPath(String uploadDestPath) {
		this.uploadDestPath = uploadDestPath;
	}

	
	public String getSM3MatchResult() {
		return SM3MatchResult;
	}

	public void setSM3MatchResult(String sM3MatchResult) {
		SM3MatchResult = sM3MatchResult;
	}

	public String getCrc() {
		return Crc;
	}

	public void setCrc(String crc) {
		Crc = crc;
	}

	public String getSourceParent() {
		return sourceParent;
	}

	public void setSourceParent(String sourceParent) {
		this.sourceParent = sourceParent;
	}

	public int getTempsize() {
		return tempsize;
	}

	public void setTempsize(int tempsize) {
		this.tempsize = tempsize;
	}
	
	public List<RequestFile> getTempFiles() {
		return tempFiles;
	}

	public void setTempFiles(List<RequestFile> tempFiles) {
		this.tempFiles = tempFiles;
	}

	public String getSourceFiletype() {
		return sourceFiletype;
	}

	public void setSourceFiletype(String sourceFiletype) {
		this.sourceFiletype = sourceFiletype;
	}

	public String getUpload_status() {
		return upload_status;
	}

	public void setUpload_status(String upload_status) {
		this.upload_status = upload_status;
	}


	public boolean isSm4() {
		return sm4;
	}

	public void setSm4(boolean sm4) {
		this.sm4 = sm4;
	}

	public String getSM3() {
		return SM3;
	}

	public void setSM3(String sM3) {
		SM3 = sM3;
	}

	public boolean isAttachment() {
		return attachment;
	}

	public void setAttachment(boolean attachment) {
		this.attachment = attachment;
	}

	public String getParent() {
		return Parent;
	}

	public void setParent(String parent) {
		Parent = parent;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public long getStarPos() {
		return starPos;
	}

	public void setStarPos(long starPos) {
		this.starPos = starPos;
	}

	public int getEndPos() {
		return endPos;
	}

	public void setEndPos(int endPos) {
		this.endPos = endPos;
	}

	public byte[] getBytes() {
		return bytes;
	}

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public String getFile_md5() {
		return file_md5;
	}

	public void setFile_md5(String file_md5) {
		this.file_md5 = file_md5;
	}
	
	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public long getLocalfile_size() {
		return localfile_size;
	}

	public void setLocalfile_size(long localfile_size) {
		this.localfile_size = localfile_size;
	}

	public long getRemotefile_size() {
		return remotefile_size;
	}

	public void setRemotefile_size(long remotefile_size) {
		this.remotefile_size = remotefile_size;
	}


	
	
	
}

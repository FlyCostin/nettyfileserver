package com.rrkd.server;

import com.rrkd.code.NettyMessageDecoder;
import com.rrkd.code.NettyMessageEncoder;
import com.rrkd.handler.DispathcherHandler;
import com.rrkd.handler.FileTransferServerHandler;
import com.rrkd.handler.SecureServerHandler;

import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.timeout.ReadTimeoutHandler;

public class FileChannelInitializer extends ChannelInitializer<Channel> {

	@Override
	protected void initChannel(Channel ch) throws Exception {
		ch.pipeline().addLast(new ReadTimeoutHandler(20));
		ch.pipeline().addLast(new ObjectEncoder());
		ch.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.weakCachingConcurrentResolver(null))); // 最大长度
		
		ch.pipeline().addLast(new NettyMessageDecoder());//设置服务器端的编码和解码
		ch.pipeline().addLast(new NettyMessageEncoder());
        
		
		
		ch.pipeline().addLast("secure",new SecureServerHandler());
		ch.pipeline().addLast("Dispatcher",new DispathcherHandler());
//		ch.pipeline().addLast(new FileCheckSM3Handler());
//		ch.pipeline().addLast("point",new FileTransferByPointHandler());
		ch.pipeline().addLast("transfer",new FileTransferServerHandler());//这个是真正意义上处理数据的类
		
	}

}

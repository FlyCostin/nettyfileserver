package com.rrkd.util;

public class Status {
	private static final String UPlOAD_STATUS_OK="FINISHED";//上传状态
	private static final String CHECK_SUCCESS="SUCCESS";//验证成功
	private static final String CHECK_FALSE="FAILE";//验证失败
	
	public static String getUploadStatusOk() {
		return UPlOAD_STATUS_OK;
	}
	public static String getCheckSuccess() {
		return CHECK_SUCCESS;
	}
	public static String getCheckFalse() {
		return CHECK_FALSE;
	}
	
}

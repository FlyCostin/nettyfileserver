package com.rrkd.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GzipUtils {
	public static byte[] gzip(byte[] data) {
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		GZIPOutputStream gzip;
		byte[] ret=null;
		try {
			gzip = new GZIPOutputStream(bos);
			gzip.write(data);
			gzip.finish();
			gzip.close();
			ret=bos.toByteArray();
			bos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public static byte[] ungzip(byte[] data) {
		ByteArrayInputStream bis=new ByteArrayInputStream(data);
		GZIPInputStream gzip;
		byte[] ret=null;
		try {
			gzip = new GZIPInputStream(bis);
			byte[] buf=new byte[1024];
			int num=-1;
			ByteArrayOutputStream bos=new ByteArrayOutputStream();
			while((num=gzip.read(buf,0,buf.length))!=-1){
				bos.write(buf,0,num);
			}
			gzip.close();
			bis.close();
			ret=bos.toByteArray();
			bos.flush();
			bos.close();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		return ret;
	}
}

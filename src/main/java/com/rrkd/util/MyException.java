package com.rrkd.util;


public class MyException extends Exception{

	
	private static final long serialVersionUID = -6332331569919530944L;
	private String detail;
	/**
	 * 构造一个基本异常
	 * @param message
	 */
	public MyException(String message){
		detail=message;
	}
	
	public String toString(){
		return "MyException["+detail+"]";
	}
}
